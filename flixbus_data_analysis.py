import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import math
from functools import reduce
from sklearn.metrics import mean_absolute_error
from sklearn import metrics
from fbprophet import Prophet
from fixbus_functions import *

def read():

	file_channels = 'case_jr_analyst/orders_channels.csv'
	file_country = 'case_jr_analyst/orders_country.csv'
	file_tickets = 'case_jr_analyst/orders_tickets.csv'

	df_channels= pd.read_csv(file_channels)
	df_country = pd.read_csv(file_country)
	df_tickets = pd.read_csv(file_tickets)

	return df_channels, df_country, df_tickets

def preprocess(df1, df2):

	df_channels = df1.dropna()
	df_channels_cleaned = df_channels.drop_duplicates(keep='first')

	df_country_cleaned = df2
	df_country_cleaned.loc[df_country_cleaned['country_1'].isnull(),'country_1'] = df_country_cleaned['country_2']
	df_country_cleaned = df_country_cleaned[pd.notnull(df_country_cleaned['country_1'])]
	df_country_cleaned.drop(['country_2'], axis=1,inplace=True)

	return df_channels_cleaned, df_country_cleaned


def task():
#### reading the csv files ####
	data = read()

	df_channels = data[0]
	df_country = data[1]
	df_tickets = data[2]

	clean_data = preprocess(df_channels, df_country)

	df_tickets.drop(['type'], axis=1, inplace=True)
	df_tickets_cleaned = df_tickets.groupby('id',sort=False).sum().reset_index()


	df_channels_cleaned = clean_data[0]

	df_country_cleaned = clean_data[1]
	df_country_cleaned = df_country_cleaned[~df_country_cleaned['country_1'].isin(['xx'])]
	df_country_cleaned['country_1'] = df_country_cleaned['country_1'].apply(pd.to_numeric)
	df_country_cleaned['country_1'] = df_country_cleaned['country_1'].apply(np.int64)
	#print (df_country_cleaned.loc[df_country_cleaned['country_1'] == '4'])


	dfs = [df_channels_cleaned, df_tickets_cleaned, df_country_cleaned]
	df_final = reduce(lambda left,right: pd.merge(left,right,on='id'), dfs)
	df_final_sortedbydate = df_final.sort_values(by='date') # dont delete

	df_final_sortedbydate['date'] = pd.to_datetime(df_final_sortedbydate['date'])
	df_final_sortedbydate['date_delta'] = (df_final_sortedbydate['date'] - df_final_sortedbydate['date'].min())  / np.timedelta64(1,'D')

###list of channle id


	ex_pre_data = extracting_predicting_data(df_final_sortedbydate) ###goes for training the model
	#print (ex_pre_data)
#### real data ####
	actual_data_last10 = actual_data_last10_days(df_final_sortedbydate)

	actual_data_last10 = actual_data_last10.reset_index()
	actual_data_last10.columns = ['ds','n_tickets']
	#print (actual_data_last10)
#### merging the actual data and predicted data for model accuracy ####
	final_dfs = [actual_data_last10, ex_pre_data]
	df_final_dfs = reduce(lambda left,right: pd.merge(left,right,on='ds'), final_dfs)
	df_final_dfs.drop(['yhat_lower'], axis=1,inplace=True)
	df_final_dfs.drop(['yhat_upper'], axis=1,inplace=True)
	#sns.distplot((df_final_dfs['n_tickets']-df_final_dfs['yhat']))
	#plt.show()
	#print ('Final check',df_final_dfs)
#### checking accuracy percentage ####
	error_cal = error_calculation(df_final_dfs)

#### predicting data for the next daily_seasonality
	predict_nxt_10days = predictions_next_days (df_final_sortedbydate)
	#total_tickets_sold = predict_nxt_10days['yhat'].round().sum()

### writing into csv ###



if __name__ == '__main__':
	task()
