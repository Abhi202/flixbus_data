import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from functools import reduce
from sklearn import metrics
from sklearn.metrics import accuracy_score
from fbprophet import Prophet



def error_calculation(df_final_dfs):

    actual_values_list = df_final_dfs['n_tickets'].tolist()
    predicted_values_list = df_final_dfs['yhat'].round().tolist()
    #df_final_dfs['yhat'] = df_final_dfs['yhat'].round().tolist()
    #df_final_dfs['n_tickets'] = df_final_dfs['n_tickets'].round().tolist()
    #print (actual_values_list)
    sns.distplot((df_final_dfs['n_tickets']-df_final_dfs['yhat']))
    #plt.show()
    ab_error = metrics.mean_absolute_error (df_final_dfs['n_tickets'],df_final_dfs['yhat'])
    sq_error = metrics.mean_squared_error (df_final_dfs['n_tickets'],df_final_dfs['yhat'])
    sqrt_error = np.sqrt(sq_error)
    #print ('Mean Absolue Error:',ab_error)
    #print ('Mean Squared Error:',sq_error)
    #print ('Root Mean Squared Error:',sqrt_error)

    df_final_dfs['n_tickets'], df_final_dfs['yhat'] = np.array(df_final_dfs['n_tickets']), np.array(df_final_dfs['yhat'])
    mean_absoulte_percentage_error = np.mean(np.abs((df_final_dfs['n_tickets'] - df_final_dfs['yhat']) / df_final_dfs['n_tickets'])) * 100
    #print ('Mean Absoulte Percentage Error:',mean_absoulte_percentage_error)
    return

def predictions_next_days(df_final_sortedbydate):

    mask = (df_final_sortedbydate['date'] >= '2017-01-01') \
        & (df_final_sortedbydate['date'] <= '2018-02-25')
    extracted_data_byDate = df_final_sortedbydate.loc[mask]

    df_final = extracted_data_byDate[['channel_id', 'country_1',
            'n_tickets', 'date']]

    channel_id_list = df_final.channel_id.unique().tolist()
    country_1_list = df_final.country_1.unique().tolist()
    #print (channel_id_list)
    for item in channel_id_list:

        for ele in country_1_list:

            try:
                df_final = extracted_data_byDate[['channel_id', 'country_1'
                        , 'n_tickets', 'date']]
                df_final = df_final.loc[df_final['channel_id'] == item,]
                df_final = df_final.loc[df_final['country_1'] == ele,]
                df_final = df_final.drop('country_1', 1)
                df_final = df_final.drop('channel_id', 1)
                df = pd.DataFrame()
                df['ds'] = df_final.date.unique()
                x = df_final.groupby(['date']).sum()
                x = x.reset_index(drop=True)
                df = df.reset_index(drop=True)
                df['y'] = x['n_tickets']
                df['ds'].max()
                ax = df.set_index('ds').plot(figsize=(12, 8))
                ax.set_ylabel('Number of Tickets')
                ax.set_xlabel('Date')

                my_model = Prophet(interval_width=0.95,
                                   daily_seasonality=True)
                my_model.fit(df)
                future_dates = my_model.make_future_dataframe(periods=11)
                future_dates.tail(11)
                forecast = my_model.predict(future_dates)

                # final_prediction=forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail(11)

                prediction = forecast[['ds', 'yhat']].tail(11)

                # print (final_prediction)
                # total_tickets_sold = final_prediction['yhat'].round().sum()
                # print ('For channel_id {} and country_1 {}, the predictions are {} and the sum is {}'.format (item,ele,final_prediction,total_tickets_sold))

                prediction['country'] = ele
                prediction['channel'] = item
                prediction.columns = ['date', 'n_tickets', 'country',
                                      'channel']
                prediction = prediction[['date', 'country', 'channel',
                                        'n_tickets']]
                prediction.n_tickets = prediction.n_tickets.round()
                prediction.to_csv('final_predicted_data.csv',sep='\t',encoding='utf-8',doublequote=False,index=False,mode='a')

            except:
                print ('This channel_id {} and country {} didnt work'.format(item,ele))
                pass



def extracting_predicting_data(df_final_sortedbydate):

    mask = (df_final_sortedbydate['date'] >= '2017-01-01') \
        & (df_final_sortedbydate['date'] <= '2018-02-16')
    extracted_data_byDate = df_final_sortedbydate.loc[mask]

    X = extracted_data_byDate[['channel_id', 'country_1']]

    df_final = extracted_data_byDate[['channel_id', 'country_1',
            'n_tickets', 'date']]
    df_final = df_final.loc[df_final['channel_id'] == 39.0,]

    df_final = df_final.loc[df_final['country_1'] == 24,]
    #print('check',df_final)
    df_final = df_final.drop('country_1', 1)
    df_final = df_final.drop('channel_id', 1)

    df = pd.DataFrame()
    df['ds'] = df_final.date.unique()
    x = df_final.groupby(['date']).sum()
    x = x.reset_index(drop=True)
    df = df.reset_index(drop=True)
    df['y'] = x['n_tickets']
    #df['y_original'] = df['y'] ###### impotant
    #df['y'] = np.log(df['y'])
    df['ds'].max()
    #print ('new DataFrame',df)
    #ax = df.set_index('ds').plot(figsize=(12, 8))
    #ax.set_ylabel('Number of Tickets')
    #ax.set_xlabel('Date')

    #plt.show()
    my_model = Prophet(interval_width=0.95,daily_seasonality=True)
    my_model.fit(df)
    future_dates = my_model.make_future_dataframe(periods=10)
    future_dates.tail(10)
    forecast = my_model.predict(future_dates)
    forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail(10)
    #my_model.plot(forecast)
    #print ('Forecast for:',forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail(10))
    return forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail(10)

def actual_data_last10_days(df_final_sortedbydate):

    mask = (df_final_sortedbydate['date'] >= '2018-02-17') \
        & (df_final_sortedbydate['date'] <= '2018-02-26')
    extracted_data_byDate = df_final_sortedbydate.loc[mask]

    df_final = extracted_data_byDate[['channel_id', 'country_1',
            'n_tickets', 'date']]
    df_final = df_final.loc[df_final['channel_id'] == 39.0,]
    df_final = df_final.loc[df_final['country_1'] == 24,]
    df_final = df_final.drop('country_1', 1)
    df_final = df_final.drop('channel_id', 1)

    df = pd.DataFrame()
    df['ds'] = df_final.date.unique()
    x = df_final.groupby(['date']).sum()

    return x
